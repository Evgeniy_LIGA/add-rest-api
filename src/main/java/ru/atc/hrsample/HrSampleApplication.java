package ru.atc.hrsample;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Insert;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import ru.atc.hrsample.dao.EmployeesMapper;

@SpringBootApplication
public class HrSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrSampleApplication.class);
    }

    @Autowired
    private EmployeesMapper employeesMapper;

    // При запуске системы автоматический заполняем таблицу jobs
    // иначе нельзя будет добавить сотрудников
    @EventListener(ApplicationReadyEvent.class)
    private void Init() {
        employeesMapper.initJobs();
    }

}
