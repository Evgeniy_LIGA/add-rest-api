package ru.atc.hrsample.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.atc.hrsample.dao.DepartmentsMapper;
import ru.atc.hrsample.dto.*;
import ru.atc.hrsample.service.api.DepartmentsService;

import java.util.List;

/**
 * РЕСТ контроллер для работы с департаментами
 */
@RestController
@RequestMapping("/departments")
@RequiredArgsConstructor
@Tag(name = "departments", description = "АПИ для департаментов")
public class DepartmentsController {

    private final DepartmentsService departmentsService;

    @GetMapping("/{departmentId}")
    @Operation(summary = "Получение департамента по идентификатору")
    public DepartmentsDto getDepartmentById(@PathVariable Integer departmentId) {
        return departmentsService.getDepartmentById(departmentId);
    }

    @PostMapping
    @Operation(summary = "Создание нового департамента")
    public DepartmentsDto insertLocation(@RequestBody DepartmentsDto dto) {
        return departmentsService.insertDepartment(dto);
    }

    @PatchMapping
    @Operation(summary = "Обновить все свойства или часть свойств департамента")
    public DepartmentsDto updateLocation(@RequestBody DepartmentsDto dto) {
        return departmentsService.updateDepartment(dto);
    }

    @GetMapping
    @Operation(summary = "Получение списка всех департаментов")
    public List<DepartmentsDto> getAll() {
        return departmentsService.getAll();
    }


    private final DepartmentsMapper departmentsMapper;

    @DeleteMapping
    @Operation(summary = "Удаление всех департаментов")
    public void deleteAll() {
        departmentsMapper.deleteAll();
    }
}
