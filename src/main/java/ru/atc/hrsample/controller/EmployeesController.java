package ru.atc.hrsample.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.atc.hrsample.dao.EmployeesMapper;
import ru.atc.hrsample.dto.EmployeesDto;
import ru.atc.hrsample.service.api.EmployeesService;

import java.util.List;

/**
 * РЕСТ контроллер для работы с наемными сотрудниками
 */
@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
@Tag(name = "employees", description = "АПИ для работы с наемными сотрудниками")
public class EmployeesController {

    private final EmployeesService employeesService;

    @GetMapping("/{employeeId}")
    @Operation(summary = "Получение сотрудника по идентификатору")
    public EmployeesDto getEmployeeById(@PathVariable Integer employeeId) {
        return employeesService.getEmployeeById(employeeId);
    }

    @PostMapping
    @Operation(summary = "Создание нового сотрудника")
    public EmployeesDto insertLocation(@RequestBody EmployeesDto dto) {
        return employeesService.insertEmployee(dto);
    }

    @GetMapping
    @Operation(summary = "Получение списка всех сотрудников")
    public List<EmployeesDto> getAll() {
        return employeesService.getAll();
    }


    private final EmployeesMapper employeesMapper;

    @DeleteMapping
    @Operation(summary = "Удаление всех сотрудников")
    public void deleteAll() {
        employeesMapper.deleteAll();
    }
}
