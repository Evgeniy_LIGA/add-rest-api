package ru.atc.hrsample.dao;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import ru.atc.hrsample.entity.*;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface DepartmentsMapper {

    @Select("select * from departments where department_id = #{id} ")
    Optional<DepartmentsEntity> getDepartmentById(Integer id);

    @Insert("insert into departments(department_id, department_name, manager_id, location_id) VALUES " +
            "(#{departmentId}, #{departmentName}, #{managerId}, #{locationId})")
    @SelectKey(keyProperty = "departmentId", before = true, resultType = Integer.class,
            statement = "select nextval('departments_seq')")
    void insert(DepartmentsEntity entity);

    // Гибкий Update если один из параметров не указан, то он не будет обновляться.
    @Update("UPDATE departments SET "+
            "department_name=COALESCE(#{departmentName},department_name),"+
            "manager_id=COALESCE(#{managerId},manager_id),"+
            "location_id=COALESCE(#{locationId},location_id) "+
            "WHERE department_id=#{departmentId};")
    void update(DepartmentsEntity entity);

    @Select("select * from departments")
    List<DepartmentsEntity> getAll();

    @Delete("ALTER SEQUENCE departments_seq RESTART WITH 1; DELETE FROM departments;")
    void deleteAll();

    @Delete("ALTER SEQUENCE departments_seq RESTART WITH 1; TRUNCATE departments CASCADE;")
    void deleteAllCascade();
}
