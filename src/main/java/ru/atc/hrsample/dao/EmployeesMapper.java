package ru.atc.hrsample.dao;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import ru.atc.hrsample.entity.EmployeesEntity;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface EmployeesMapper {

    @Select("select * from employees where employee_id = #{id} ")
    Optional<EmployeesEntity> getEmployeeById(Integer id);

    @Insert("insert into employees("+
            "employee_id,     first_name,   last_name,   email,   phone_number,   hire_date,   job_id,     salary,   manager_id,   department_id ) VALUES " +
            "(#{employeeId}, #{firstName}, #{lastName}, #{email}, #{phoneNumber}, #{hireDate}, #{jobId}, #{salary}, #{managerId}, #{departmentId} )")
    @SelectKey(keyProperty = "employeeId", before = true, resultType = Integer.class,
            statement = "select nextval('employees_seq')")
    void insert(EmployeesEntity entity);

    @Select("select * from employees")
    List<EmployeesEntity> getAll();

    @Delete("ALTER SEQUENCE employees_seq RESTART WITH 1; DELETE FROM employees;")
    void deleteAll();

    @Delete("ALTER SEQUENCE employees_seq RESTART WITH 1; TRUNCATE employees CASCADE;")
    void deleteAllCascade();

    // Безопасный Insert, даже если кто-то удалит часть строк они восстановятся при следующем запуске
    @Insert("INSERT INTO jobs VALUES " +
            "('gen_id','General management',120000,220000)," +
            "('prod_id','Production',40000,120000)," +
            "('dev_id','Development',60000,200000)," +
            "('market_id','Marketing',80000,120000)," +
            "('pchase_id','Purchasing',80000,100000)," +
            "('sales_id','Sales',120000,170000)," +
            "('eco_id','Ecology',40000,60000)," +
            "('int_id','Intelligence',120000,220000)" +
            " ON CONFLICT DO NOTHING ;")
    void initJobs();
}
