package ru.atc.hrsample.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Модель департамента
 */
@Data
@Schema(title = "Модель данных департамента")
public class DepartmentsDto {
    @Schema(title = "Идентификатор департамента")
    private Integer departmentId;

    @Schema(title = "Название департамента")
    private String departmentName;

    @Schema(title = "Идентификатор менеджера", required = true)
    private Integer managerId;

    @Schema(title = "Идентификатор локации", required = true)
    private Integer locationId;
}