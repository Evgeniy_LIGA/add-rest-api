package ru.atc.hrsample.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.sql.Date;

/**
 * Модель наемного сотрудника
 */
@Data
@Schema(title = "Модель данных наемного сотрудника")
public class EmployeesDto {
    @Schema(title = "Идентификатор сотрудника")
    private Integer employeeId;

    @Schema(title = "Имя сотрудника")
    private String firstName;

    @Schema(title = "Фамилия сотрудника", required = true)
    private String lastName;

    @Schema(title = "Электронная почта", required = true)
    private String email;

    @Schema(title = "Телефон")
    private String phoneNumber;

    @Schema(title = "Дата приема на работу", required = true)
    private Date hireDate;

    @Schema(title = "Идентификатор типа работы", required = true)
    private String jobId;

    @Schema(title = "Заработная плата")
    private Integer salary;

    @Schema(title = "Идентификатор менеджера")
    private Integer managerId;

    @Schema(title = "Идентификатор департамента")
    private Integer departmentId;
}
