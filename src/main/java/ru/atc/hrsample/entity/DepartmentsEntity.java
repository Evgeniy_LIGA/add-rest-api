package ru.atc.hrsample.entity;

import lombok.Data;

@Data
public class DepartmentsEntity {
    private Integer departmentId;
    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
