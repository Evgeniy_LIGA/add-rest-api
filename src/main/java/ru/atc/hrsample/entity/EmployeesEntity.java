package ru.atc.hrsample.entity;

import java.sql.Date;
import lombok.Data;

@Data
public class EmployeesEntity {
    private Integer employeeId;     // not null default nextval('employees_seq')
    private String firstName;       // varchar(20)
    private String lastName;        // varchar(25) NOT NULL
    private String email;           // varchar(25) NOT NULL, CONSTRAINT emp_email_uk  UNIQUE (email)
    private String phoneNumber;     // varchar(20)
    private Date hireDate;          // date        NOT NULL
    private String jobId;           // varchar(10) NOT NULL
    private Integer salary;         // int, CONSTRAINT emp_salary_min CHECK (salary > 0)
    private Integer managerId;      // int
    private Integer departmentId;   // int
}
