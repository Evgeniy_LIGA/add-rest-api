package ru.atc.hrsample.service.api;

import ru.atc.hrsample.dto.DepartmentsDto;

import java.util.List;

/**
 * Сервис для работы с департаментами
 */
public interface DepartmentsService {

    /**
     * Получает сущность локации по ид
     *
     * @param id идентификатор
     * @return сохраненная сущность с ид.
     */
    DepartmentsDto getDepartmentById(Integer id);

    DepartmentsDto insertDepartment(DepartmentsDto dto);

    DepartmentsDto updateDepartment(DepartmentsDto dto);

    List<DepartmentsDto> getAll();
}
