package ru.atc.hrsample.service.api;

import ru.atc.hrsample.dto.EmployeesDto;

import java.util.List;

/**
 * Сервис для работы с наемными сотрудниками
 */
public interface EmployeesService {

    /**
     * Получает сущность локации по ид
     *
     * @param id идентификатор
     * @return сохраненная сущность с ид.
     */
    EmployeesDto getEmployeeById(Integer id);

    EmployeesDto insertEmployee(EmployeesDto dto);

    List<EmployeesDto> getAll();
}
