package ru.atc.hrsample.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.hrsample.dao.DepartmentsMapper;
import ru.atc.hrsample.dto.DepartmentsDto;
import ru.atc.hrsample.entity.DepartmentsEntity;
import ru.atc.hrsample.service.api.DepartmentsService;
import ru.atc.hrsample.service.api.LocationsService;

import java.util.List;

/**
 * Реализация сервиса по работе с локациями
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DepartmentsServiceImpl implements DepartmentsService {

    private final DepartmentsMapper departmentsMapper;
    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public DepartmentsDto getDepartmentById(Integer id) {
        DepartmentsEntity locationsEntity = departmentsMapper.getDepartmentById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность локации по идентификатору=%s", id)
                )
        );
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(locationsEntity, DepartmentsDto.class);
    }

    @Override
    @Transactional
    public DepartmentsDto insertDepartment(DepartmentsDto dto) {
        DepartmentsEntity departmentsEntity = modelMapper.map(dto, DepartmentsEntity.class);
        departmentsMapper.insert(departmentsEntity);
        log.info("Создана сущность с id={}", departmentsEntity.getDepartmentId());
        return getDepartmentById(departmentsEntity.getDepartmentId());
    }

    @Override
    @Transactional
    public DepartmentsDto updateDepartment(DepartmentsDto dto) {
        DepartmentsEntity departmentsEntity = modelMapper.map(dto, DepartmentsEntity.class);
        departmentsMapper.update(departmentsEntity);
        log.info("Обновлена сущность с id={}", departmentsEntity.getDepartmentId());
        return getDepartmentById(departmentsEntity.getDepartmentId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<DepartmentsDto> getAll() {
        List<DepartmentsEntity> entities = departmentsMapper.getAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }
}
