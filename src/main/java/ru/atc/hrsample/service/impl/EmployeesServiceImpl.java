package ru.atc.hrsample.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.hrsample.dao.EmployeesMapper;
import ru.atc.hrsample.dto.EmployeesDto;
import ru.atc.hrsample.entity.EmployeesEntity;
import ru.atc.hrsample.service.api.EmployeesService;

import java.util.List;

/**
 * Реализация сервиса по работе с локациями
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesMapper employeesMapper;
    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public EmployeesDto getEmployeeById(Integer id) {
        EmployeesEntity locationsEntity = employeesMapper.getEmployeeById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность локации по идентификатору=%s", id)
                )
        );
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(locationsEntity, EmployeesDto.class);
    }

    @Override
    @Transactional
    public EmployeesDto insertEmployee(EmployeesDto dto) {
        EmployeesEntity employeesEntity = modelMapper.map(dto, EmployeesEntity.class);
        employeesMapper.insert(employeesEntity);
        log.info("Создана сущность с id={}", employeesEntity.getEmployeeId());
        return getEmployeeById(employeesEntity.getEmployeeId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeesDto> getAll() {
        List<EmployeesEntity> entities = employeesMapper.getAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }
}
