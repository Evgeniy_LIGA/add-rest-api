CREATE TABLE locations (
                           location_id     integer,
                           street_address  varchar(40),
                           city            varchar(30),
                           PRIMARY KEY (location_id)
);
CREATE TABLE departments (
                             department_id   integer,
                             department_name varchar(30),
                             manager_id      integer,
                             location_id     integer,
                             PRIMARY KEY (department_id)
);
CREATE TABLE jobs (
                      job_id          varchar(10),
                      job_title       varchar(35),
                      min_salary      integer,
                      max_salary      integer,
                      PRIMARY KEY (job_id)
);
CREATE TABLE employees (
                           employee_id     integer,
                           first_name      varchar(20),
                           last_name       varchar(25),
                           email           varchar(25),
                           phone_number    varchar(20),
                           hire_date       date,
                           job_id          varchar(10),
                           salary          integer,
                           manager_id      integer,
                           department_id   integer,
                           PRIMARY KEY (employee_id)
);
CREATE TABLE job_history (
                             employee_id     integer,
                             start_date      date,
                             end_date        date,
                             job_id          varchar(10),
                             department_id   integer,
                             PRIMARY KEY (employee_id,start_date)
);
--
ALTER TABLE locations
    ALTER COLUMN city SET NOT NULL;
--
ALTER TABLE jobs
    ALTER COLUMN job_title SET NOT NULL;
--
ALTER TABLE departments
    ADD FOREIGN KEY (location_id)
        REFERENCES locations;
ALTER TABLE departments
    ADD FOREIGN KEY (manager_id)
        REFERENCES employees(employee_id);
--
ALTER TABLE departments
    ALTER COLUMN department_name SET NOT NULL;
--
ALTER TABLE employees
    ADD FOREIGN KEY (department_id)
        REFERENCES departments(department_id);
ALTER TABLE employees
    ADD FOREIGN KEY (job_id)
        REFERENCES jobs;
ALTER TABLE employees
    ADD FOREIGN KEY (manager_id)
        REFERENCES employees(employee_id);
--
ALTER TABLE employees
    ADD CONSTRAINT email_unique UNIQUE (email);
ALTER TABLE employees
    ALTER COLUMN last_name SET NOT NULL;
ALTER TABLE employees
    ALTER COLUMN email SET NOT NULL;
ALTER TABLE employees
    ALTER COLUMN hire_date SET NOT NULL;
ALTER TABLE employees
    ALTER COLUMN job_id SET NOT NULL;
--
ALTER TABLE job_history
    ADD FOREIGN KEY (department_id)
        REFERENCES departments;
ALTER TABLE job_history
    ADD FOREIGN KEY (employee_id)
        REFERENCES employees;
ALTER TABLE job_history
    ADD FOREIGN KEY (job_id)
        REFERENCES jobs;
--
ALTER TABLE job_history
    ALTER COLUMN start_date SET NOT NULL;
ALTER TABLE job_history
    ALTER COLUMN end_date SET NOT NULL;
ALTER TABLE job_history
    ALTER COLUMN job_id SET NOT NULL;
-- Первыми заполняем таблицы без ссылок на PRIMARY KEY
-- Локации:
INSERT INTO locations VALUES
(1, 'Mitinskaya Ul., bld. 45', 'Moscow'),
(2, 'Gagarina Ul., bld. 13', 'Angarsk'),
(3, 'Okruzhnaya Ul., bld. 49', 'Ufa'),
(4, 'Aviatsionnaya Ul., bld. 33', 'Brest'),
(5, 'Universitetskaya, bld. 28', 'Novosibirsk');
-- И виды работ:
INSERT INTO jobs VALUES
('gen_id','General management',120000,220000),
('prod_id','Production',40000,120000),
('dev_id','Development',60000,200000),
('market_id','Marketing',80000,120000),
('pchase_id','Purchasing',80000,100000),
('sales_id','Sales',120000,170000),
('eco_id','Ecology',40000,60000);
-- Логично первым делом создать департаменты
INSERT INTO departments VALUES
(1,'General department',null,1),
(2,'Production department',null,2),
(3,'Development department',null,1),
(4,'Marketing department',null,3),
(5,'Purchasing department',null,4),
(6,'Sales department',null,1),
(7,'Ecology department',null,5);
-- Далее заполняем сотрудников
INSERT INTO employees VALUES
(1,'Elisey','Pavlenko','epavlenko@mail.ru','89165262436',date '2000-09-28','gen_id',200000,1,1),
(2,'Petr','Petrov','ppetr@mail.ru','89177643478',date '2010-04-07','prod_id',100000,2,2),
(3,'Ivan','Ivanovich','iivan@mail.ru','89175463478',date '2001-09-28','dev_id',150000,3,3),
(4,'Sergey','Semin','serg@mail.ru','8918533678',date '2007-04-11','market_id',115000,4,4),
(5,'Valeriy','Semin','vsemin@mail.ru','8918546558',date '2008-02-24','pchase_id',90000,5,5),
(6,'Alexey','Semin','asemin@mail.ru','8918786678',date '2012-03-15','sales_id',160000,6,6),
(7,'Michael','Stepanov','mstep@mail.ru','89185563228',date '2015-05-04','eco_id',50000,7,7);
-- В конце заполняем историю работ
INSERT INTO job_history VALUES
(1,date '2000-09-28',date(now()),'gen_id',1),
(2,date '2010-04-07',date(now()),'prod_id',2),
(3,date '2001-09-28',date(now()),'dev_id',3),
(4,date '2007-04-11',date(now()),'market_id',4),
(5,date '2008-02-24',date(now()),'pchase_id',5),
(6,date '2012-03-15',date(now()),'sales_id',6),
(7,date '2015-05-04',date(now()),'eco_id',7);
-- Переносим employees id -> departments null
UPDATE departments
SET manager_id = emp.employee_id
FROM employees emp
WHERE departments.department_id=emp.department_id
-- Создадим скрытых сотрудников которые не прикреплены ни к какому отделу.
INSERT INTO jobs VALUES
('int_id','Intelligence',120000,220000);
INSERT INTO employees VALUES
(8,'Agent','Cooper','cooper@topsecretmail.ru','89180000000',date '2000-01-01','int_id',100000,1,null),
(9,'Agent','Snowden','snowden@topsecretmail.ru','89180000001',date '2000-01-01','int_id',100000,1,null);
